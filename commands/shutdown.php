<?php
if($help) return "!shutdown";
if(!$admin) return "Insufficient rights";

posix_kill($pid, SIGKILL);
echo "Shutting down\n";
ircDisconnect($socket, "Manual shutdown");
