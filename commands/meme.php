<?php
if($help) return [
    "Command" => "meme",
    "Syntax" => "!meme",
    "Description" => "Returns random post from /r/ProgrammerHumour",
    "Arguments" => []];

$data = file_get_contents("http://reddit.com/r/ProgrammerHumor/random.json");
$json = json_decode($data)[0]->data->children[0]->data->permalink;
$json = explode("/", $json)[4];
return "https://redd.it/".$json;
