<?php
if($help) return [
    "Command" => "source",
    "Syntax" => "!source",
    "Description" => "Returns link to bot's source code",
    "Arguments" => []];

return "https://gitlab.com/MrDarkHooD/irc-php-bot";
