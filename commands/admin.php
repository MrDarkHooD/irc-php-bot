<?php
if($help) return [
    "Command" => "admin",
    "Syntax" => "!admin {option} [password]",
    "Description" => "Admin information seeker and login command",
    "Arguments" => ["First argument is option for action",
                     "Possible options: ami (Am I (admin)), list (list admins), login (gain admin priviledges)",
                     "Second argument is password and only required if option is login",]];

if(!$arguments) return "Arguments required";

$action = array_shift($arguments_array);

if($action == "ami") return ($admin) ? "You are admin" : "You are not admin";
if($action == "list") return "My admins: ".implode(", ", $adminarr);

if($action == "login") {
    if(isset($blockadmin)) return "Admin login locked";
    if($adminfloodblock[$nick]) return "You will not get admin rights.";
    
    $password = implode(" ", $arguments_array);
	if($password != botpassword) {
        if(!isset($adminattemp[$nick])) $adminattemp[$nick] = 0;
		if(!isset($adminfloodblock[$nick])) $adminfloodblock[$nick] = 0;
		
		$adminattemp[$nick]++;
		echo "Attempt: ".$adminattemp[$nick]."\n";
		echo $adminfloodblock[$nick];
		if($adminattemp[$nick] > 2) {
			$adminfloodblock[$nick] = 1;
			echo "Added $nick to admin flood protection list\n";
		}
        return "Wrong password";
    }else {
        $adminarr[] = $nick;
        echo "Added $nick to admin\n";

        if(substr($channel, 0, 1) == "#") {
            $blockadmin = 1;
            return ircToBold(ircToRed("Admin register locked. Change password and restart me."));
        }
            
        return "You are now in control of me";
	}
}
