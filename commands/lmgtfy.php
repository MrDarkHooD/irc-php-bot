<?php
if($help) return [
    "Command" => "lmgtfy",
    "Syntax" => "!lmgtfy {str}",
    "Description" => "Returns \"Let me google that for you\" link",
    "Arguments" => ["Everything after command is consider as search string"]];
              
if(!$arguments) return "Arguments required";

return "https://lmgtfy.com/?q=".urlencode($arguments);
