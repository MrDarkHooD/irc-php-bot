<?php
if($help) return [
    "Command" => "dice",
    "Syntax" => "!dice [number]",
    "Description" => "Gives random number(s) from dice",
    "Arguments" => ["First argument is amount of dices to throw. Limit is 100."]];

if($arguments > 100) return "Limited to 100";

if(is_numeric($arguments)) {
    for($i=0, $num=0; $i<$arguments; $i++) {
        $rand = rand(1,6);
        $num += $rand;
        $output .= $rand;
        $output = ($i<$arguments-1) ? $output .= "+" : $output .= "=".$num;
    }
    return $output;
}
return rand(1,6);
