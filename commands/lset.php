<?php
if($help) return "!lset {setting} {option} (settings: location, notify)";

list($target, $option) = $arguments_array;
if($target == "location") {
    $query = $db->prepare("UPDATE usersettings SET location = ? WHERE nick = ?");
    $query->execute(array($option, $nick));
    return "Your location is updated to $option";
}elseif($target == "notify" && ($option == 0 || $option == 1)) {
    $query = $db->prepare("UPDATE usersettings SET notify = ? WHERE nick = ?");
    $query->execute(array($option, $nick));
    return ($option) ? "Notifactions on" : "Notifactions off";
}
