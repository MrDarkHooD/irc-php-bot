<?php
if($help) return "!say {channel} {message}";
if(!$admin) return "Insufficient rights";

$channel = array_shift($arguments_array);
$tosay = implode(" ", $arguments_array);
ircSay($socket, $channel, $tosay);
return "";
