<?php
if($help) return [
    "Command" => "title",
    "Syntax" => "This command activates automatically when detecting url.",
    "Description" => "Returns title(s) of given link(s)",
    "Arguments" => ["This command works with !nsfw"]];

if(strpos($msg, "!nsfw") !== false)
    ircSay($socket, $channel, ircToBold(ircToRed("^^^ NSFW ^^^")));
unset($title);
if(!function_exists("progressf")) {
    function progressf($ch, $str) {
        global $title;
        global $sitedata;
        $sitedata .= $str;
        $title="";
        if(preg_match("/\<title\>(.*)\<\/title\>/i", $sitedata, $title)) {
            $sitedata="";
            $title = htmlspecialchars_decode(html_entity_decode($title[1], ENT_QUOTES | ENT_XML1, 'UTF-8'));
            if(strlen($title) > 124) $title = substr($title, 0, 124)."...";
            return 0;
        }
        if(mb_strlen($str, '8bit') > 100000000) return 0;
        return strlen($str);
    }
}

if(count($arguments_array) > 2)
    $arguments_array = array_slice($arguments_array, 0, 2);

foreach(array_unique($arguments_array) as $url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RANGE, '0-100000000');
    curl_setopt($ch, CURLOPT_NOSIGNAL, false);
    curl_setopt($ch, CURLOPT_USERAGENT, botuseragent);
    curl_setopt($ch, CURLOPT_BUFFERSIZE, 128);
    curl_setopt($ch, CURLOPT_NOPROGRESS, false);
    curl_setopt($ch, CURLOPT_WRITEFUNCTION, 'progressf');
    $data = curl_exec($ch);
    $curl_errno = curl_errno($ch);
    $curl_error = curl_error($ch);
    curl_close($ch);
    
    if($curl_errno > 0) {
        echo ircToRed("Error ").$curl_errno."\n";
        echo $curl_error."\n";
    }

    if($title)
        ircSay($socket, $channel, $title);
}
return "";
