<?php
if($help) return "!restart";
if(!$admin) return "Insufficient rights";

echo "Restarting\n";
ircDisconnect($socket, "Manual restart");
