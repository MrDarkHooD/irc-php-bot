<?php
if($help) return [
    "Command" => "date",
    "Syntax" => "!date [option]",
    "Description" => "Time and date stuff.",
    "Arguments" => ["dtm: Days in this mont.",
                    "dty: Days in this year",
                    "doy: Day of the year",
                    "day: Weekday now",]];

switch($arguments_array[0]) {
case "dtm": //days this month
    return date("t");
    break;
case "dty": //Days this year
    return (date('t', mktime(0, 0, 0, 2, 1, date("Y"))) == 29) ? 366 : 365;
    break;
case "doy": //Day of year
    return date("z")+1;
    break;
case "day": //Weekday now
    return strftime("%A");
    break;
default:
    return date("Y-m-d H:i:sP");
    break;
}
