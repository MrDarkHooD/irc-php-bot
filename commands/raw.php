<?php
if($help) return "!raw {str}";
if(!$admin) return "Insufficient rights";
if(!$arguments) return "Arguments required";

ircRaw($socket, $arguments);
