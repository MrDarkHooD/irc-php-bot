<?php
if($help) return [
    "Command" => "servertime",
    "Syntax" => "!servertime",
    "Description" => "Returns bot's servers time",
    "Arguments" => []];
              
return (new DateTime())->format('Y-m-d\TH:i:s.uO');
