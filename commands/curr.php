<?php
if($help) return [
    "Command" => "curr",
    "Syntax" => "!curr {amount} {from} {to}",
    "Description" => "Currency converter",
    "Arguments" => ["First argument is amount.",
                    "Second argument is base currency",
                    "Third argument is destination currency",]];

$delimeter = (isset($arguments_array[3])) ? $arguments_array[3] : 3;
list($amount, $oc, $fc) = $arguments_array;
$crypto = ["xrp","ltc","eth","btc"];
$err=0;
if(in_array($oc,$crypto) || in_array($fc,$crypto)) {
	$res = ($oc == "btc") ? $fc : $oc;
	$order = (in_array($oc,$crypto) && in_array($fc,$crypto)) ? $res."btc" : "btc".$res;
	if(!$json = file_get_contents("https://www.bitstamp.net/api/v2/ticker/$order/"))
		$err = 1;
		$json = json_decode($json)->last;
		$result = ($oc=="btc") ? ($amount*$json) : ((1/$json)*$amount);
	}else {
		$oc = strtoupper($oc);
		$fc = strtoupper($fc);
		if(!$json = file_get_contents("https://api.exchangeratesapi.io/latest?base=$oc&symbols=$fc"))
			$err=1;
		$json = json_decode($json)->rates->$fc;
		$result = ($amount*$json);
	}
	if(!$err) {
		if(is_numeric($amount) && $amount > 0 && $oc != $fc)
			return $amount.get_currency_symbol($oc)." = ".round($result, $delimeter).get_currency_symbol($fc);
}else {
	return "Currency not supported";
}
