<?php
if($help) return [
    "Command" => "convert",
    "Syntax" => "!{number} {from} {to} OR !convert {str} {from} {to}",
    "Description" => "Unit converter",
    "Arguments" => ["First argument is amount",
                    "Second argument is base unit",
                    "Third argument is target unit",]];

list($num, $from, $to) = $arguments_array;
	
if($from == "f" && $to == "m") return round(($num/100*12*2.54), 3)."m";
if($from == "i" && $to == "m") return round(($num/100*2.54), 3)."m";

if($from == "c" && $to == "f") return $num."°C = ".round(($num * 9/5) + 32, 2)."°F";
if($from == "f" && $to == "c") return $num."°F = ".round(($num - 32) / 1.8, 2)."°C";
    
if($from == "m") {
    if($to == "i") return round(($num/2.54*100), 3)."″";
    if($to == "f") return round(($num/2.54/12*100), 3)."′";
    if($to == "km")  return round(($num*1000), 3)."km";
    if($to == "cm")  return round(($num/100), 3)."cm";
    if($to == "mm")  return round(($num/1000), 3)."mm";
}

if($from == "decimal" && $to == "bit") return decbin($arguments);
if($from == "bit" && $to == "decimal") return bindec($arguments);

if($from == "hex" && $to == "decimal") return hexdec($num);
if($from == "decimal" && $to == "hex") return dechex($num);

if($from == "hex" && $to == "ascii") return hex2bin(str_replace("\\x", "", $arguments));
if($from == "ascii" && $to == "hex") {
    foreach(str_split($arguments) as $char)
        $output .= '\\x'.bin2hex($char);
    return $output;
}
