<?php
if($help) return [
    "Command" => "weather",
    "Syntax" => "!weather [location]",
    "Description" => "Seek weather of chosen location",
    "Arguments" => ["First argument is location",
                     "If argument is not given it first seeks location from settings and if not found it seeks it by ip gained from host data",]];

if($arguments) {
	$city = $arguments;
}else {
	$query = $db->prepare("SELECT location FROM usersettings WHERE nick = ?");
	$query->execute(array(strtolower($nick)));
	$city = $query->fetch()[0];
	if(empty($city)) {
		preg_match('/@(\S+)/', $data, $host);
		$ip = gethostbyname($host[1]);
		$details = json_decode(file_get_contents("http://ipinfo.io/$ip/json"));
		$city = $details->city;
	}
}

$url = "https://api.openweathermap.org/data/2.5/weather?lang=fi&units=metric&q=".urlencode($city)."&appid=".$apikeys['weather'];
if(is_url_valid($url)) {
	$jsondata = json_decode(file_get_contents($url));
	if(is_numeric($jsondata->main->temp))
        return ucfirst($city).": ".$jsondata->weather[0]->description." ".round($jsondata->main->temp,1)."°C";
}else {
	return "Location not valid ";
}
