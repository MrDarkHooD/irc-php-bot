<?php
if($help) return "!action {channel} {options}";
if(!$admin) return "Insufficient rights";
if(!$arguments) return "Arguments required";

$channel = array_shift($arguments_array);
$arguments = implode(" ", $arguments_array);
ircAction($socket, $channel, $arguments);
