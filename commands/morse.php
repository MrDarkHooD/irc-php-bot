<?php
if($help) return [
    "Command" => "morse",
    "Syntax" => "!morse {str}",
    "Description" => "Changes strin to morse code",
    "Arguments" => ["Everything after command is changed to morse. Works with letters from a to z.",]];

if(!$arguments) return "Arguments required";

$letters = range("a", "z");
$morse = ['·- ','-··· ','-·-· ','-·· ','· ','··-· ','--· ','···· ','·· ','·--- ','-·- ','·-·· ','-- ','-· ','--- ','·--· ','--·- ','·-· ','··· ','- ','··- ','···- ','·-- ','-··- ','-·-- ','--·· '];

return str_replace($letters, $morse, $arguments);
