 <?php
if($help) return [
    "Command" => "regex",
    "Syntax" => "s/{regex}/[result]/[flags] [msg history count]",
    "Description" => "Regex replacer for messages in short history",
    "Arguments" => ["You should know regex to use this, I am not gonna teach you it."]];

if(substr($msg, 0, 2) != "s/") return "That command wont work, see !regex -h";

list($regex, $change, $flags) = array_slice(explode("/", $msg), 1);
$msgarr = explode(" ", $flags);
$toeditnum = (is_numeric(end($msgarr))) ? end($msgarr)-1 : 0;

$flags = preg_replace("/[^isx]/", "", $flags);
$original = $msg_history[count($msg_history)-2-$toeditnum];
$newphrase = preg_replace("/".$regex."/".$flags, $change, $original);
return ($original != $newphrase) ? $newphrase : "Nothing changed";
