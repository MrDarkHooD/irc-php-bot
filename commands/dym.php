<?php
if($help) return [
    "Command" => "dym",
    "Syntax" => "!dym {str} [results amount]",
    "Description" => "Returns google's \"Did you mean\" suggestions.",
    "Arguments" => ["First argument is string to seek, it is \" compatible",
    "Second argument is amount of results it gives"]];

if(is_numeric(end($arguments_array)) && count($arguments_array)>1) {
	$results_amount = array_pop($arguments_array);
	$arguments = implode(" ", $arguments_array);
}else $results_amount = 1;
	
if($arguments[0] == "\"" && $arguments[-1] == "\"") $arguments = substr($arguments, 1, -1);
	
$xml = simplexml_load_file('http://suggestqueries.google.com/complete/search?output=toolbar&hl=en&q='.$arguments);
foreach($xml->CompleteSuggestion as $result) $suggestions[] = $result->suggestion["data"];
	
if($suggestions[0] == $arguments) array_shift($suggestions);
	
if($suggestions[0]) {
	if($results_amount > count($suggestions)) $results_amount = count($suggestions);
	$output = "Did you mean: " . implode(", ", array_slice($suggestions, 0, $results_amount));
}else $output = "No results";

unset($suggestions);
return $output;
