<?php
if($help) return [
    "Command" => "timein",
    "Syntax" => "!timein {str}",
    "Description" => "Tells what time is in spesific timezone",
    "Arguments" => ["First argument is timezone",
                     "If timezone is only hald of data, it tries to seek it from list",]];

if(!$arguments) return "Arguments required";

if(!strstr($arguments, "/") && !in_array(substr($arguments, 0, 1), ["+", "-"]))
	foreach(timezone_identifiers_list() as $timezone)
		if(strstr($timezone, "/"))
			if(explode("/", $timezone)[1] == $arguments) $arguments = $timezone;

if(in_array($arguments, timezone_identifiers_list()) ||
    is_numeric(substr($arguments, 1))) {
    $date = new DateTime(null, new DateTimeZone($arguments));
    return $date->format('Y-m-d H:i:sP');
}else return "Invalid timezone";
