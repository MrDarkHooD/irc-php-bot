<?php
if($help) return "!blockcommand {commands}";
if(!$admin) return "Insufficient rights";
if(!count($arguments_array)) return "Arguments required";
foreach($arguments_array as $command) {
    $query = $db->prepare("INSERT INTO commandblock (command) VALUES (?)");
    $query->execute(array($command));
}
