<?php
if($help) return [
    "Command" => "startuptime",
    "Syntax" => "!startuptime",
    "Description" => "Tells when bot has started",
    "Arguments" => []];

$diff = round((microtime(true)-$_SERVER['REQUEST_TIME_FLOAT'])/60);
return "My startup time was $starttime ($diff min ago)";
