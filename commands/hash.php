<?php
if($help) return [
    "Command" => "hash",
    "Syntax" => "!hash {algo} {str}",
    "Description" => "Hashes string with chosen algorithm",
    "Arguments" => ["First argument is algorithm.",
                     "Second argument is string to convert",]];

$algo = array_shift($arguments_array);
if($output = hash($algo, implode(" ", $arguments_array))) {
    return $output;
}else return "Algorithm not valid";
