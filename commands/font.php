<?php
if($help) return [
    "Command" => "font",
    "Syntax" => "!font {font} {str}",
    "Description" => "Return string with new font.",
    "Arguments" => ["First argument is name of font.",
                    "Fonts: oldenglish, oldenglishbold, superscript, mirror",
                    "Second argument is text to be changed to another font",]];

if(!$arguments_array[1]) return "Not enaught arguments";

$font = array_shift($arguments_array);
$letters = range("a", "z");
$original = implode(" ", $arguments_array);
switch($font) {
	case "oldenglish":
		$newfont = ['𝔞','𝔟','𝔠','𝔡','𝔢','𝔣','𝔤','𝔥','𝔦','𝔧','𝔨','𝔩','𝔪','𝔫','𝔬','𝔭','𝔮','𝔯','𝔰','𝔱','𝔲','𝔳','𝔴','𝔵','𝔶','𝔷'];
		break;
	case "oldenglishbold":
		$newfont = ['𝖆','𝖇','𝖈','𝖉','𝖊','𝖋','𝖌','𝖍','𝖎','𝖏','𝖐','𝖑','𝖒','𝖓','𝖔','𝖕','𝖖','𝖗','𝖘','𝖙','𝖚','𝖛','𝖜','𝖝','𝖞','𝖟'];
		break;
	case "superscript":
		$letters = array_merge(range("a", "z"), range(0, 9));
		$newfont = ['ᵃ','ᵇ','ᶜ','ᵈ','ᵉ','ᶠ','ᵍ','ʰ','ᶦ','ʲ','ᵏ','ˡ','ᵐ','ⁿ','ᵒ','ᵖ','ᵠ','ʳ','ˢ','ᵗ','ᵘ','ᵛ','ʷ','ˣ','ʸ','ᶻ','¹','²','³','⁴','⁵','⁶','⁷','⁸','⁹','⁰'];
		break;
	case "mirror":
		$original = strrev($original);
		$newfont = ['ɒ','d','ɔ','b','ɘ','ʇ','ϱ','ʜ','i','į','ʞ','l','m','n','o','q','p','ɿ','ƨ','Ɉ','υ','v','w','x','γ','z'];
		break;
	default:
		return "Font not supported";
		break;
}

if($output == $original) return "nothing changed";
return str_replace($letters, $newfont, $original);
