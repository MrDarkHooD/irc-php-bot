<?php
if($help) return [
    "Command" => "trout",
    "Syntax" => "!trout {nick} [object]",
    "Description" => "Slap another user",
    "Arguments" => ["First argument is user to slap.",
                     "Second argument is to choose with what to slap",]];

if(!$arguments) return "Arguments required";
if(empty($arguments_array[1])) $arguments_array[1] = "large trout";

$who = array_shift($arguments_array);
$with = implode(" ", $arguments_array);
return $nick." slaps $who around a bit with a ".$with;
