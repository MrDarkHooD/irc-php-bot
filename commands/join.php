<?php
if($help) return "!join {channel}";
if(!$admin) return "Insufficient rights";
if(!$arguments) return "Arguments required";

ircJoin($socket, $arguments);
echo "Joining to channel $arguments\n";
