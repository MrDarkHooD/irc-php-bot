<?php
if($help) return [
    "Command" => "calc",
    "Syntax" => "!calc {mathematical string}",
    "Description" => "Calculator",
    "Arguments" => ["Everything after command is consider as mathematical string. Works with +, -, * and /. () are not working.",]];

$arguments = preg_replace("/ /", "", $arguments);
$numbers = preg_split( "/(\+|-|\*|\/)/", $arguments );
$output = $numbers[0];
$calc = -1;
$delimeters = [];
    
for($i=0; $i<count($numbers); $i++) {
    $calc += strlen($numbers[$i])+1;
    $delimeters[] = substr($arguments, $calc,1);
}

for($i=1; $i<count($delimeters); $i++) {

    switch($delimeters[$i-1]) {
    case '+':
        $output = $output + $numbers[$i];
        break;
    case '-':
        $output = $output - $numbers[$i];
        break;
    case '*':
        $output = $output * $numbers[$i];
        break;
    case '/':
        $output = $output / $numbers[$i];
        break;
    }
}

if($arguments != preg_replace("/[^0-9+-\/%*\s]/", "", $arguments))
    $output = "Format of argument is wrong";

return $output;
