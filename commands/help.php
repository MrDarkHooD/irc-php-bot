<?php
if($help) return [
    "Command" => "help",
    "Syntax" => "!help",
    "Description" => "Returns link to bot usage help page",
    "Arguments" => []];

return boturl."help.html";
