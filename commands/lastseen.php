<?php
if($help) return [
    "Command" => "lastseen",
    "Syntax" => "!lastseen {nick}",
    "Description" => "Returns last timestamp and message from spesific user.",
    "Arguments" => ["First argument is nick of user.",]];

$query = $db->prepare("SELECT * FROM log WHERE nick = ? AND channel = ? ORDER BY id DESC LIMIT 1");
$query->execute(array($arguments, $channel));
if(!$line = $query->fetch()) return "No logs for $arguments";
return $line['nick']." seen last time {$line['time']} with message \"{$line['msg']}\"";
