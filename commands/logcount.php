<?php
if($help) return [
    "Command" => "logcount",
    "Syntax" => "!logcount {str}",
    "Description" => "Returns count of messages where string is found",
    "Arguments" => ["First argument is string to search",]];

if(substr($channel,0,1) != "#") return "This works only on channels";

$query = $db->prepare("SELECT COUNT(id) FROM log WHERE msg LIKE ? AND channel = ?");
$query->execute(array("%$arguments%", $channel));
return $query->fetchColumn();
