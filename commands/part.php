<?php
if($help) return "!part [channel]";
if(!$admin) return "Insufficient rights";

if(!$arguments) $arguments = $channel;

ircPart($socket, $arguments);
echo "Parting from channel $arguments\n";
