<?php
if($help) return [
    "Command" => "topsayers",
    "Syntax" => "!topsayers {str}",
    "Description" => "Returns nick of 3 users who mentiened string most times",
    "Arguments" => ["First argument is string to check",]];

$query = $db->prepare("SELECT COUNT(id) AS count, nick
FROM log 
WHERE msg LIKE ? AND channel = ? 
GROUP BY nick 
ORDER BY count DESC
LIMIT 3");

$query->execute(array("%$arguments%", $channel));
$output = "Top sayers for string $arguments: ";
while($line = $query->fetch()) {
	$line["nick"] = substr($line["nick"], 0, 1) . ircToBold("") . substr($line["nick"], 1);
	$output .= $line["nick"] . " (".$line["count"].") ";
}
return $output;
