<?php
if($help) return "!allowcommand {commands}";
if(!$admin) return "Insufficient rights";
if(!count($arguments_array)) return "Arguments required";
foreach($arguments_array as $command) {
    $query = $db->prepare("DELETE FROM commandblock WHERE command = ?");
    $query->execute(array($command));
}
