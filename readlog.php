<?php
include("settings.php");
include("functions.php");

try {
    $db = new PDO($pdofile);
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

function printMsg($id, $msg, $time, $nick, $channel, $i, $search) {
	$msg = preg_replace("%#(.*) %i", tocyan("#$1 "), $msg);
	$msg = str_replace($search, tored($search), $msg);
	echo $i.". ". $id ." ".$time." ";
	echo tocyan($channel)." ";
	echo togreen($nick).": " . $msg."\n";
}

while(1) {
	try {
		$limit=$search=$nick=$limit=$channel=$search=null;
		$i=0;
		$result=[];
		$action = strtolower(readline(tobold("Action (S=Search C=Count I=ID default=S): ")));
		if(!$action) $action = "s";
		if($action != "i") {
			$channel = readline(tobold("Channel: "));
			$search = ($action == "s") ? readline(tobold("Search: ")) : readline(tobold("String to count: "));
			$nick = readline(tobold("Nick: "));
		}else {
			$id = readline(tobold("ID: "));
			$limit = readline(tobold("Limit (default=20): "));
			$limit = ($limit) ? $limit : 20;
		}
		switch ($action) {
				
			case "s":
				$limit = readline(tobold("Limit (default=20): "));
				$limit = ($limit) ? $limit : 20;
				echo "---Searching from ".(($channel) ? tocyan("#".$channel) : "logs");
				if($nick) echo " from user ".$nick;
				if($search) echo " word ".tored($search);
				echo " $limit last queries.\n";

				$query = $db->prepare("SELECT * FROM log WHERE msg LIKE ? AND channel LIKE ? AND nick LIKE ? ORDER BY ID DESC LIMIT ?");
				$query->execute(array("%$search%", "%$channel%", "%$nick%", $limit));
				while($line = $query->fetch()) $result[] = $line;
				foreach(array_reverse($result) as $line) {
					printMsg($line['ID'], $line["msg"], $line["time"], $line["nick"], $line["channel"], $i, $search);
					$i++;
				}
				echo ($i) ? "---End of results, $i found.\n" : "---No results\n";
				break;
				
			case "c":
				$query = $db->prepare("SELECT COUNT(id) FROM log WHERE msg LIKE ? AND channel LIKE ? AND nick LIKE ?");
				$query->execute(array("%$search%", "%$channel%", "%$nick%"));
				echo $query->fetchColumn()." results for ".(($search) ? tored($search) : toviolet("null"));
				echo (($channel) ? " in channel ".tocyan("#".$channel):"").(($nick) ?" from user ".togreen($nick):"")."\n";
				break;
				
			case "i":
				$query = $db->prepare("SELECT * FROM log LIMIT ? OFFSET ?");
				$query->execute(array($limit, $id-1));
				while($line = $query->fetch()) $result[] = $line;
				foreach($result as $line) {
					printMsg($line['ID'], $line["msg"], $line["time"], $line["nick"], $channel, $i, $search);
					$i++;
				}
				break;

		}
	} catch (Exception $e) {
		echo tored("Fatal error: ") . $e . togreen("\nRecovering")."\n";	
	}
}