<?php
function tored($string) {
	return "\033[31m$string\033[0m";
}

function toviolet($string) {
	return "\033[35m$string\033[0m";
}

function togreen($string) {
	return "\033[32m$string\033[0m";
}

function tocyan($string) {
	return "\033[96m$string\033[0m";
}

function tobold($string) {
	return "\033[1m$string\033[22m";
}

function ircToRed($string) {
	return "\x0304$string\x03";
}

function ircToBold($string) {
	return "\x02$string\x02";
}

function ircToUnderline($string) {
	return "\x1F$string\x1F";
}

function ircToItalic($string) { //doesn't work on most clients
	return "\x1D$string\x1D";
}

function ircToStrikethrough($string) { //doesn't work on most clients
	return "\x1E$string\x1E";
}

function ircToMonospace($string) { //doesn't work on most clients
	return "\x11$string\x11";
}

function ircSanitize($input) {
	return str_replace(["\n", "\r", "\r\n", "\t", "\v", "\e", "\f"], ' ', $input);
}

function get_currency_symbol($cc = 'USD') {
	$cc = strtoupper($cc);
	$currency = array(
		"USD" => "$",  //U.S. Dollar
		"BRL" => "R$", //Brazilian Real
		"CAD" => "C$", //Canadian Dollar
		"EUR" => "€",  //Euro
		"ILS" => "₪",  //Israeli New Sheqel
		"INR" => "₹",  //Indian Rupee
		"JPY" => "¥",  //Japanese Yen 
		"PHP" => "₱",  //Philippine Peso
		"PLN" => "zł", //Polish Zloty
		"GBP" => "£",  //Pound Sterling
		"TWD" => "$",  //Taiwan New Dollar 
		"TRY" => "₺",  //Turkish Lira
		"BTC" => "฿",  //Bitcoin
		"XMR" => "ɱ",  //Monero
		"LTC" => "Ł",  //Litecoin
		"ETH" => "Ξ"   //Ethereum
	);
	return (array_key_exists($cc, $currency)) ? $currency[$cc] : $cc;
}

function is_url_valid($url) {

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10000);
    $data = curl_exec($ch);
    $curl_errno = curl_errno($ch);
    $curl_error = curl_error($ch);
    curl_close($ch);

    if ($curl_errno > 0) {
        return "cURL Error ($curl_errno): $curl_error\n";
    } else {
        return $data;
    }

    
}

function get_channel($buffer) {
	if($buffer == "") return null;
	if(strstr($buffer, "PRIVMSG")) {
		$channel = explode(" :", explode("PRIVMSG ", $buffer)[1])[0];
		if(strstr(explode("PRIVMSG ", $buffer)[0], "NOTICE")) return null;
		if($channel == $GLOBALS["botnick"])
			$channel = explode("!", substr($buffer, 1))[0];
		return $channel;
	}else return null;
}

function get_nick($buffer) {
	if($buffer == "") return null;
	if(strstr($buffer,"PRIVMSG")) {
		return explode("!",explode(":",$buffer)[1])[0];
	}else return null;
}

function get_msg($buffer) {
	if($buffer == "") return null;
	if(strstr($buffer, "PRIVMSG")) {
		$channel = explode(" :", explode("PRIVMSG ", $buffer)[1])[0];
		return implode("PRIVMSG $channel :", array_slice(explode("PRIVMSG $channel :",$buffer), 1));
	}else return null;
}

function ircDisconnect($socket, $reason = "Probably error") {
	if($socket == "") return null;
	$reason = ircSanitize($reason);
	fwrite($socket,"QUIT $reason\r\n");
	fclose($socket);
	die("User interrupt\n");
}

function ircJoin($socket, $channel) {
	if($socket == "") return null;
	if($channel == "") return null;
	$channel = ircSanitize($channel);
	fwrite($socket, "JOIN $channel\r\n");
}

function ircPart($socket, $channel) {
	if($socket == "") return null;
	if($channel == "") return null;
	$channel = ircSanitize($channel);
	fwrite($socket, "PART $channel\r\n");
}

function ircSay($socket, $channel, $msg) {
	if($socket == "") return null;
	if($channel == "") return null;
	if($msg == "") return null;
	$channel = ircSanitize($channel);
	$msg = ircSanitize($msg);
	if(strlen($msg) > 255) $msg = "Message was too long";
	fwrite($socket, "PRIVMSG $channel :$msg\r\n");
	echo date("H:i:s ") . tored("Sent to $channel: $msg\n");
}

function ircRaw($socket, $str) {
	if($str == "") return null;
	$str = ircSanitize($str);
	fwrite($socket, $str."\r\n");
}

function ircAction($socket, $channel, $str) {
	if($socket == "") return null;
	if($channel == "") return null;
	if($str == "") return null;
	$channel = ircSanitize($channel);
	$str = ircSanitize($str);
	fwrite($socket, "PRIVMSG $channel :\x01ACTION $str\x01\r\n");
}

function ircAway($socket, $str=null, $disable=true) {
	if($socket == "") return null;
	$str = ircSanitize($str);
	if($disable)
		fwrite($socket, "AWAY\r\n");
	else
		fwrite($socket, "AWAY :$str\r\n");
}

function ircSetMode($socket, $channel, $user, $mode) {
	if($socket == "") return null;
	if($channel == "") return null;
	if($user == "") return null;
	if($mode == "") return null;
	$user = ircSanitize($user);
	$mode = ircSanitize($mode);
	fwrite($socket, "MODE $channel $mode $user\r\n");
}

function ircKick($socket, $user, $from, $reason = "") {
	if($socket == "") return null;
	if($channel == "") return null;
	if($from == "") return null;
	$user = ircSanitize($user);
	$from = ircSanitize($from);
	$reason = ircSanitize($reason);
	fwrite($socket,"KICK $from $user :$reason\r\n");
}

function ircTopic($socket, $channel, $topic) {
	if($socket == "") return null;
	if($channel == "") return null;
	if($topic == "") return null;
	$channel = ircSanitize($channel);
	$topic = ircSanitize($topic);
	fwrite($socket, "TOPIC $channel :$topic\r\n");
}

function ircNick($socket, $nick) {
	if($socket == "") return null;
	if($nick == "") return null;
	$nick = ircSanitize($nick);
	fwrite($socket, "NICK $nick\r\n");
}
