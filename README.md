You do not want to use this, trust me.

Originated from this: https://www.ohjelmointiputka.net/keskustelu/24835-php-irc-luokka/sivu-1#v239204

This is designed to be used from terminal on Linux.
Requires PHP7+

<br>
SQlite table create commands:

```sql
CREATE TABLE log (
  ID INTEGER PRIMARY KEY AUTOINCREMENT,
  nick text,
  channel text,
  msg text,
  time TEXT
);
CREATE TABLE usersettings (
  ID INTEGER PRIMARY KEY AUTOINCREMENT,
  nick text,
  location text,
  notify text DEFAULT NULL
);
CREATE TABLE commandblock (
  ID INTEGER PRIMARY KEY AUTOINCREMENT,
  command text
);
``` 

For sake of this repo I keep settings.php here but it will actually be included from /../ircbot_settings.php to prevent accidental push.
Database will be included from /../databases/irc_bot.db

