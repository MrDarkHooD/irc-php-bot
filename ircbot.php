<?php
declare(ticks = 1);
pcntl_signal(SIGINT, "signal_handler");
function signal_handler($signal) {
	if($signal == SIGINT) echo "Ctrl C\n";
}
set_time_limit(0);
require "functions.php"; 
require "../ircbot_settings.php";

if(empty(botnick)) die("Set botnick in settings.php");
if(empty(botname)) die("Set botname in settings.php");
if(empty(botaddress)) die("Set botaddress in settings.php");
if(empty(botport)) die("Set botport in settings.php");
if(empty(botchannel)) die("Set botchannel in settings.php");
if(empty(botpassword)) die("Set botpassword in settings.php");

$db = new PDO($pdofile);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$serverip = gethostbyname(gethostname());
if(!$socket = fsockopen(botaddress, botport))
	die("Error while connectin\n");

fwrite($socket, "USER ".botnick." $serverip $serverip :".botname."\r\n");
fwrite($socket, "NICK ".botnick." $serverip\r\n");

$pid = file_get_contents("pid.txt");
$pid = str_replace(["\n", "\r", "\r\n"], "", $pid);

$msg_history = $adminarr = $reminder = $user_variables = [];
$firsttime = 1;
$lastcheckdate = date("d");
$starttime = date("Y-m-d H:i:s");
$default_timezone = date_default_timezone_get();
$botnick = botnick;

while(1) {
    if(is_bool($socket))
	    die("\$socket died\n");
	
    if(feof($socket))
        die("feof error\n");
    
    if(!empty($socket))
        $data = trim(fgets($socket, 4096));

    if(substr($data, 0, 6) == "PING :")
        fwrite($socket, "PONG :".substr($data, 6)."\r\n");
	
    if($firsttime) {
        ircJoin($socket, botchannel);
        $firsttime = 0;
    }

    $output = null;
    $channel = get_channel($data);
    $nick = get_nick($data);
    $msg = get_msg($data);
    $admin = (in_array($nick, $adminarr)) ? true : false;

    //This makes sure there is not millions of messages in history array
    if(count($msg_history) > 10) $msg_history = array_slice($msg_history, 5);

    //Log handler
    if(trim($nick) != "" && trim($channel) != "" && trim($msg) != "") {
        $msg_history[] = $msg;

        $query = $db->prepare("INSERT INTO log (channel, nick, msg, time) VALUES (?, ?, ?, ?)");
        $query->execute(array($channel, $nick, $msg, date("d-m-Y H:i:s")));
	
        echo date("H:i:s ") . tocyan($channel) . " ~ " . togreen("($nick)") . " ".$msg."\n";

        //This bot creates user profile to everyone who posts message it can read, we do it here
        $query = $db->prepare("SELECT * FROM usersettings WHERE nick = ?");
        $query->execute(array($nick));
        if(!$query->fetch()) {
            $query = $db->prepare("INSERT INTO usersettings (nick, location, notify) VALUES (?, ?, ?)");
            $query->execute(array($nick, NULL, NULL));
        }
    }elseif(!empty($data)) echo trim($data)."\n";

    if(strtolower($msg) == "kiitos rikka")
        ircSay($socket, $channel, "Ole hyvä ".$nick);

    if(substr($msg,0,1) == "!" && substr_count($msg," ")==0) {
        $query = $db->prepare("SELECT action FROM alias WHERE user = ? AND command = ? ORDER BY ID DESC LIMIT 1");
        $query->execute(array($nick, explode("!",$msg)[1]));
        
	if($comm = $query->fetch()['action']) {
		$arguments_for_alias = explode(" ",$msg);
		array_shift($arguments_for_alias);
            $msg = $comm." ".implode(" ", $arguments_for_alias);
}
    }
    
    if(substr_count($msg, " | ")) $pipe_parts = explode(" | ", $msg);
    foreach(explode(" | ", $msg) as $pipe_now) {
        
        if(strstr($pipe_now, "\$pipe"))
            $pipe_now = str_replace("\$pipe", $output, $pipe_now);
        else
            $pipe_now .= " ".$output;

        foreach($user_variables as $user_variable)
            $pipe_now = str_replace($user_variable[0], $user_variable[1], $pipe_now);

        ircSay($socket, $channel, implode(" -> ", $pipe_now));
        $message_parts = explode(" ", $pipe_now);
        $command = preg_replace("/[^!\-0-9a-z]/", "", $message_parts[0]);
        if(substr($command, 0, 1) == "!") {
            $command = substr($command, 1, strlen($command)-1);
            $arguments_array = array_slice($message_parts, 1);
            $arguments_array = array_filter($arguments_array,"strlen");
            $arguments = implode(" ", $arguments_array);
            $arguments = trim($arguments);

            if(substr($arguments_array[0],0,1) == "-") {
                foreach($arguments_array as $arr_argument) {
                    if(substr($arr_argument,0,1) == "-") {
                        $arrkey = substr($arr_argument,1);
                        if(!isset($alfa_arguments_array[$arrkey])) $alfa_arguments_array[$arrkey] = [];
                    }else {
                        array_push($alfa_arguments_array[$arrkey], $arr_argument);
                    }
                }
                foreach($alfa_arguments_array as $alfa_key => $alfa_argument)
                    $alfa_arguments_array[$alfa_key] = implode(" ", $alfa_arguments_array[$alfa_key]);
            }

        }else $command = null;

        //This is little hack to wish new day at midnight, finnish language
        if(date("H:i") == "00:00" && $lastcheckdate != date("d")) {
            $lastcheckdate = date("d");
            setlocale(LT_TIME, "fi_FI.UTF-8");

            $date = new DateTime();
            $week = $date->format("W");
            $pdate = $date->format("Y-m-d");
            $week_day_name = $date->format("l");

            $output = "Tänään on " . $week_day_name . " " . $ptime . " (viikko " . $week . ")";
#            $output += " Nimipäivää viettävät Kyösti, Kustaa sekä Kustavi.";
#            $output += "Aurinko nousee Helsingissä " . $sun_rise_time . " ja laskee " . $sun_down_time . ", päivän pituus on " . $day_lenght_hours . "h " . $day_lenght_minutes . "min.";

#            $output = "Hyvää ".strftime("%A").((date("w") == 3) ? "a!" : "ta!"); //English: $output = "Good ".date("l")."!";
            if(strtolower($output) == strtolower(end($msg_history))) $output = "Tuhma!"; //this is just goofing
            ircSay($socket, botchannel, $output);
            $output = "";
        }

        //This is hack for changin numeric values (example meters to inches)
        if(is_numeric($command)) {
            array_unshift($arguments_array, $command);
            $command = "convert";
        }

        //Automatic title hack
        if(preg_match_all('%(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})%', $msg, $urls)) {
            if($urls[0][0]) {
                $command = "title";
                foreach($urls[0] as $url)
                    $arguments_array[] = $url;
            }
        }

        if(substr($msg, 0, 2) == "s/" && substr_count($msg, "/") >= 3) {
            $command = "regex";
        }
        
        if($command) {

            echo "Command: $command|\n";
            echo "Arguments: $arguments|\n";
        
            if(is_file("commands/$command.php")) {

                $query = $db->prepare("SELECT * FROM commandblock WHERE command = ?");
                $query->execute(array($command));
                if(!$query->fetch()) {
                    $help = ($arguments_array[0] == "-h") ? true : false;
                    $output = include("commands/$command.php");
                    if(is_array($output)) {
                        if(!$arguments_array[1]) $arguments_array[1] = "s";

                        switch($arguments_array[1]) {
                        case "s":
                            $output = $output["Syntax"];
                            break;
                        case "d":
                            $output = $output["Description"];
                            break;
                        case "a":
                            if(count($output["Arguments"])) $output = implode(", ", $output["Arguments"]);
                            break;
                        default:
                            $output = "Not avaibled";
                            break;
                        }
                    }
                }else $output = "Blocked command";
            }
        }
    }
    $user_variables = [];
    $arguments="";
    $arguments_array=[];
    $alfa_arguments_array = [];
    ircSay($socket, $channel, $output);
    /*if($output != null) {
        //if(strlen($output) > 255) $channel = $nick;
        ircSay($socket, $channel, $output);
    }*/

    if(!empty($data) && empty($msg)) {
        //Delete user from admins if leaves channel
        if(strpos($data, "PART") !== false) $part = explode("PART ", $data);
        if(strpos($data, "QUIT") !== false) $part = explode("QUIT ", $data);
        if(strpos($data, "NICK") !== false) $part = explode("NICK ", $data);
        if(isset($part[1])) {
            $nick = substr($data, 1, (strpos($data, "!")-1));
            if(in_array($nick, $adminarr)) {
                $key = array_search($nick, $adminarr);
                unset($adminarr[$key]);
                echo "Removed $nick from admins\n";
            }
        }
    }
    
}
ircDisconnect($socket);
die("End\n");
